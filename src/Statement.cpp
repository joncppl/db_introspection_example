//
// Created by Jonathan on 9/27/2020.
//

#include <acc/db/Database.hpp>
#include <acc/db/Statement.hpp>

#include "../sqlite3/sqlite3.h"

namespace acc::db
{

Statement::Statement(Database & database, const std::string & query)
{
  const auto ec = sqlite3_prepare_v3(
    database.m_db,
    query.c_str(),
    static_cast<int>(query.size()),
    0,
    &m_stmt,
    nullptr
  );

  if (SQLITE_OK != ec) {
    throw DatabaseStatementPrepareError(
      fmt::format(R"(Failed to prepare statement "{}": {} {})", query, sqlite3_errstr(ec), sqlite3_errmsg(database.m_db)));
  }
}

Statement::~Statement()
{
  if (m_stmt) {
    sqlite3_finalize(m_stmt);
  }
}

ResultRows Statement::execute(Database & database)
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  ResultRows ret;
  int ec = SQLITE_OK;
  do {
    ec = sqlite3_step(m_stmt);
    switch (ec) {
      case SQLITE_BUSY:
        throw DatabaseStatementExecutionError("BUSY");
      case SQLITE_ROW: {
        RowResult row;
        const auto columns = sqlite3_column_count(m_stmt);
        for (auto i = 0; i < columns; i++) {
          const auto name = sqlite3_column_name(m_stmt, i);
          const auto type = sqlite3_column_type(m_stmt, i);
          switch (type) {
            case SQLITE_NULL:
              row[name] = nullptr;
              break;
            case SQLITE_INTEGER: {
              // but seriously, why though?
              FieldValue fv;
              fv.emplace<int64_t>(sqlite3_column_int64(m_stmt, i));
              row.emplace(name, fv);
              break;
            }
            case SQLITE_FLOAT:
              row[name] = sqlite3_column_double(m_stmt, i);
              break;
            case SQLITE_TEXT:
              row[name] = std::string(
                reinterpret_cast<const char *>(sqlite3_column_text(m_stmt, i)));
              break;
            case SQLITE_BLOB: {
              sqlite3_column_blob(m_stmt, i);
              const auto size = sqlite3_column_bytes(m_stmt, i);
              std::vector<uint8_t> blob(size);
              std::memcpy(blob.data(), sqlite3_column_blob(m_stmt, i), size);
              row[name] = blob;
              break;
            }
            default:
              throw DatabaseStatementExecutionError(
                fmt::format("Failed to retrieve column: {} in {}", sqlite3_errmsg(database.m_db), sql()));
          }
        }
        ret.emplace_back(row);
        break;
      }
      case SQLITE_ERROR:
      case SQLITE_MISUSE:
        throw DatabaseStatementExecutionError(sqlite3_errmsg(database.m_db));
      default:
        if (ec != SQLITE_DONE) {
          if (ec == SQLITE_CONSTRAINT) {
            throw DatabaseConstraintError(fmt::format("{} in \"{}\"", sqlite3_errmsg(database.m_db), sql()));
          } else {
            throw DatabaseStatementExecutionError(fmt::format("{} in \"{}\"", sqlite3_errstr(ec), sql()));
          }
        }
    }
  } while (ec != SQLITE_DONE);
  reset();
  return ret;
}

void Statement::reset()
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  sqlite3_reset(m_stmt);
}

void Statement::clear_bindings()
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  sqlite3_clear_bindings(m_stmt);
}

void Statement::bind(int bind_index, DBInteger value)
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  const auto ec = sqlite3_bind_int64(m_stmt, bind_index, value);

  if (SQLITE_OK != ec)
    throw DatabaseStatementBindError(sqlite3_errstr(ec));
}

void Statement::bind(int bind_index, DBReal value)
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  const auto ec = sqlite3_bind_double(m_stmt, bind_index, value);

  if (SQLITE_OK != ec)
    throw DatabaseStatementBindError(sqlite3_errstr(ec));
}

void Statement::bind(int bind_index, const DBText & value)
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  const auto ec = sqlite3_bind_text(m_stmt,
    bind_index,
    value.c_str(),
    static_cast<int>(value.size()),
    nullptr);

  if (SQLITE_OK != ec)
    throw DatabaseStatementBindError(sqlite3_errstr(ec));
}

void Statement::bind(int bind_index, const DBBlob & value)
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  const auto ec = sqlite3_bind_blob(m_stmt,
    bind_index,
    value.data(),
    static_cast<int>(value.size()),
    nullptr);

  if (SQLITE_OK != ec)
    throw DatabaseStatementBindError(sqlite3_errstr(ec));
}

void Statement::bind_null(int bind_index)
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  const auto ec = sqlite3_bind_null(m_stmt, bind_index);

  if (SQLITE_OK != ec)
    throw DatabaseStatementBindError(sqlite3_errstr(ec));
}

std::string Statement::sql() const
{
  if (!m_stmt)
    throw DatabaseNullStatementError("");

  const auto raw = sqlite3_expanded_sql(m_stmt);
  std::string r(raw);
  sqlite3_free(raw);
  return r;
}

}