//
// Created by Jonathan on 9/26/2020.
//

#include <acc/db/Database.hpp>
#include <acc/db/Meta.hpp>

#include "../sqlite3/sqlite3.h"

namespace acc::db
{

Database::Database(const std::string & database_file)
{
  const auto ec = sqlite3_open_v2(
    database_file.c_str(),
    &m_db,
    SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX,
    nullptr
  );
  if (SQLITE_OK != ec) {
    m_db = nullptr;
    throw DatabaseInitializeError(fmt::format("Failed to open database: {}", sqlite3_errstr(ec)));
  }
  spdlog::info("Opened database at {}", database_file);

  execute_query("PRAGMA foreign_keys = ON;");

  if (register_table<Meta>()) {
    Meta meta {SCHEMA_VERSION};
    meta.insert(*this);
    spdlog::info("Database schema version is {}", SCHEMA_VERSION);
  } else {
    const auto schema_version = get_schema_version();
    spdlog::info("Database schema version is {}", schema_version);
    if (schema_version != SCHEMA_VERSION) {
      throw DatabaseInitializeError(fmt::format("Schema version mismatch {} != {}",
        schema_version,
        SCHEMA_VERSION));
    }
  }
}

Database::~Database()
{
  if (m_db) {
    spdlog::info("Closing connection to database");
    const auto ec = sqlite3_close_v2(m_db);
    m_db = nullptr;
    if (SQLITE_OK != ec) {
      spdlog::error("Failed to open database: {}", sqlite3_errstr(ec));
    }
  }
}

ResultRows Database::execute_query(const std::string & query)
{
  Statement stmt(*this, query);
  return stmt.execute(*this);
}

size_t Database::get_schema_version()
{
  const auto meta_entries = Meta::list(*this);
  if (meta_entries.size() != 1) {
    throw DatabaseInitializeError("There must be exactly 1 Meta entry");
  }
  return meta_entries[0].schema_version;
}

std::set<std::string> Database::current_tables()
{
  const auto current_tables = execute_query("SELECT * FROM `sqlite_master` WHERE `type`='table';");
  std::set<std::string> table_names;
  std::transform(current_tables.begin(),
    current_tables.end(),
    std::inserter(table_names, table_names.begin()),
    [](const RowResult & rr) {
        return std::get<std::string>(rr.at("name"));
    });
  return table_names;
}

bool Database::table_exists(const std::string & table_name)
{
  return current_tables().count(table_name);
}

}