//
// Created by joncp on 5/11/2020.
//

#include <gtest/gtest.h>

#include <acc/db/Tables.hpp>
#include <acc/db/Meta.hpp>

using namespace acc::db;

class TestDatabase : public ::testing::Test
{
protected:
  constexpr static const char DATABASE[] = "test_db.sqlite3";

  void SetUp() override
  {
    remove(DATABASE);
  }

  void TearDown() override
  {
    remove(DATABASE);
  }
};

TEST_F(TestDatabase, TestCreateAndConnect)
{
  Database database(DATABASE);
}

TEST_F(TestDatabase, TestMetaQueryStrings)
{
  ASSERT_EQ(
    "CREATE TABLE `META` (`id` INTEGER PRIMARY KEY ASC UNIQUE NOT NULL, `schema_version` INTEGER NOT NULL);",
    Meta::create_table_query());
  ASSERT_EQ("INSERT INTO `META` (`schema_version`) VALUES (?);", Meta::insert_query());
}

TEST_F(TestDatabase, TestSchemaVersion)
{
  Database database(DATABASE);
  ASSERT_EQ(database.get_schema_version(), SCHEMA_VERSION);
}

struct TestTable
{
  DBInteger id;
  DBReal v1;
  DBText s1;

  DB_STRUCT(TestTable, "test_table",
    DB_FIELD_ID_AUTO(id),
    DB_FIELD_NOTNULL_NO_DEFAULT(v1),
    DB_FIELD_NOTNULL_NO_DEFAULT(s1)
  )
};

TEST_F(TestDatabase, TestCreateTable)
{
  Database database(DATABASE);

  database.create_table<TestTable>();
}

TEST_F(TestDatabase, TestInsertAndSelect)
{
  Database database(DATABASE);

  database.create_table<TestTable>();
  database.execute_query("INSERT INTO test_table (v1, s1) VALUES (1.1, \"This is a test\");");
  database.execute_query("INSERT INTO test_table (v1, s1) VALUES (2.2, \"This is a test 2\");");
  auto query_result = database.execute_query("SELECT * from test_table");
  ASSERT_EQ(query_result.size(), 2);
  ASSERT_DOUBLE_EQ(std::get<DBReal>(query_result[0]["v1"]), 1.1);
  ASSERT_EQ(std::get<DBText>(query_result[0]["s1"]), "This is a test");
  ASSERT_EQ(std::get<DBInteger>(query_result[0]["id"]), 1);
  ASSERT_DOUBLE_EQ(std::get<DBReal>(query_result[1]["v1"]), 2.2);
  ASSERT_EQ(std::get<DBText>(query_result[1]["s1"]), "This is a test 2");
  ASSERT_EQ(std::get<DBInteger>(query_result[1]["id"]), 2);
}

struct TestTable2
{
  DBInteger id;
  DBInteger x;

  DB_STRUCT(TestTable2, "test_table2",
    DB_FIELD_ID_AUTO(id),
    DB_FIELD_NOTNULL_NO_DEFAULT(x)
  )
};

struct TestTable3
{
  DBInteger id;
  DBInteger other;

  DB_STRUCT(TestTable3, "test_table3",
    DB_FIELD_ID_AUTO(id),
    DB_FIELD_NOTNULL_NO_DEFAULT(other),
    DB_FOREIGN_KEY(other, TestTable2, id, ForeignKeyAction::Restrict, ForeignKeyAction::Restrict)
  )
};

TEST_F(TestDatabase, TestForeignKey)
{
  Database database(DATABASE);

  database.register_table<TestTable2>();
  database.register_table<TestTable3>();

  TestTable2 x {};
  ASSERT_EQ(1, x.insert(database));
  ASSERT_EQ(1, x.id);

  TestTable2 x2 {};
  ASSERT_EQ(2, x2.insert(database));
  ASSERT_EQ(2, x2.id);

  TestTable3 y {};
  y.other = 1;
  ASSERT_EQ(1, y.insert(database));
  ASSERT_EQ(1, y.id);

  ASSERT_THROW(database.execute_query("DELETE FROM `test_table2` WHERE id=1;"),
    DatabaseConstraintError);
}

TEST_F(TestDatabase, TestAnimeTableQueryStrings)
{
  std::cout << MALAnimeTable::create_table_query() << '\n';
  std::cout << MALAnimeTable::insert_query() << '\n';

  std::cout << MALListStatusTable::create_table_query() << '\n';
  std::cout << MALListStatusTable::insert_query() << '\n';

  std::cout << AnimeTable::create_table_query() << '\n';
  std::cout << AnimeTable::insert_query() << '\n';
}

TEST_F(TestDatabase, TestAnimeTables)
{
  Database database(DATABASE);

  database.register_table<MALAnimeTable>();
  database.register_table<MALListStatusTable>();
  database.register_table<AnimeTable>();

  MALAnimeTable mal_anime {};
  mal_anime.id = 1000;
  mal_anime.title = "Code Geass";

  ASSERT_EQ(1000, mal_anime.insert(database));

  MALListStatusTable list_status {};
  list_status.id = mal_anime.id;

  ASSERT_EQ(1000, list_status.insert(database));

  AnimeTable anime {};
  anime.mal_id = list_status.id;

  ASSERT_EQ(1, anime.insert(database));


  auto mal_anime_r = MALAnimeTable::get(database, 1000);
  ASSERT_TRUE(mal_anime_r.has_value());
  ASSERT_EQ(mal_anime_r.value().title, "Code Geass");

  mal_anime_r->main_picture = "thisisatest";

  mal_anime_r->update(database);

  auto mal_anime_r2 = MALAnimeTable::get(database, 1000);
  ASSERT_TRUE(mal_anime_r2.has_value());
  ASSERT_EQ(mal_anime_r2.value().title, "Code Geass");
  ASSERT_EQ(mal_anime_r2.value().main_picture, "thisisatest");

  mal_anime_r2->do_delete(database);

  ASSERT_FALSE(MALAnimeTable::get(database, 1000));

  std::cout << '\n';
}

TEST_F(TestDatabase, TestUpsert)
{
  Database database(DATABASE);

  database.register_table<MALAnimeTable>();

  MALAnimeTable mal_anime {};
  mal_anime.id = 1000;

  ASSERT_EQ(1000, mal_anime.upsert(database).value());

  ASSERT_TRUE(MALAnimeTable::get(database, 1000));

  MALAnimeTable mal_anime_2 {};
  mal_anime_2.id = 2000;
  ASSERT_EQ(2000, mal_anime_2.upsert(database).value());

  mal_anime.title = "LuckyStar";
  ASSERT_EQ(std::nullopt, mal_anime.upsert(database));

  const auto r_anime = MALAnimeTable::get(database, 1000);
  ASSERT_TRUE(r_anime.has_value());
  ASSERT_EQ(r_anime->title, "LuckyStar");

  std::cout << '\n';
}

TEST_F(TestDatabase, TestList)
{
  Database database(DATABASE);

  database.register_table<MALAnimeTable>();

  ASSERT_EQ(MALAnimeTable::count(database), 0);

  MALAnimeTable mal_anime {};
  mal_anime.id = 1000;
  mal_anime.title = "Code Geass";
  mal_anime.insert(database);
  ASSERT_EQ(MALAnimeTable::count(database), 1);
  mal_anime.id++;
  mal_anime.title = "LuckyStar";
  mal_anime.insert(database);
  ASSERT_EQ(MALAnimeTable::count(database), 2);
  mal_anime.id++;
  mal_anime.title = "Baccano";
  mal_anime.insert(database);
  ASSERT_EQ(MALAnimeTable::count(database), 3);

  const auto all_anime = MALAnimeTable::list(database);
  ASSERT_EQ(all_anime.size(), 3);
  ASSERT_EQ(all_anime[0].id, 1000);
  ASSERT_EQ(all_anime[0].title, "Code Geass");
  ASSERT_EQ(all_anime[1].id, 1001);
  ASSERT_EQ(all_anime[1].title, "LuckyStar");
  ASSERT_EQ(all_anime[2].id, 1002);
  ASSERT_EQ(all_anime[2].title, "Baccano");
}
