//
// Created by jonathan on 2020-08-28.
//

#ifndef ANIME_COMMAND_CENTER_TABLES_HPP
#define ANIME_COMMAND_CENTER_TABLES_HPP

#include <acc/db/db_struct_impl/declaration.hpp>

namespace acc::db
{

struct MALAnimeTable
{
  DBInteger id;
  DBText alternative_titles;
  DBInteger average_episode_duration;
  std::optional<DBText> broadcast;
  DBText created_at;
  std::optional<DBText> end_date;
  DBText main_picture;
  std::optional<DBReal> mean;
  DBText media_type;
  DBText nsfw;
  DBInteger num_episodes;
  DBInteger num_favorites;
  DBInteger num_list_users;
  DBInteger num_scoring_users;
  DBInteger popularity;
  std::optional<DBInteger> rank;
  std::optional<DBText> start_date;
  std::optional<DBText> start_season;
  DBInteger status;
  DBText synopsis;
  DBText title;
  DBText updated_at;
  std::optional<DBText> background;

  DB_STRUCT(MALAnimeTable, "mal_anime",
    DB_FIELD_ID(id),
    DB_FIELD_NOTNULL_NO_DEFAULT(alternative_titles),
    DB_FIELD_NOTNULL_NO_DEFAULT(average_episode_duration),
    DB_FIELD_NULLABLE_NO_DEFAULT(broadcast),
    DB_FIELD_NOTNULL_NO_DEFAULT(created_at),
    DB_FIELD_NULLABLE_NO_DEFAULT(end_date),
    DB_FIELD_NOTNULL_NO_DEFAULT(main_picture),
    DB_FIELD_NULLABLE_NO_DEFAULT(mean),
    DB_FIELD_NOTNULL_NO_DEFAULT(media_type),
    DB_FIELD_NOTNULL_NO_DEFAULT(nsfw),
    DB_FIELD_NOTNULL_NO_DEFAULT(num_episodes),
    DB_FIELD_NOTNULL_NO_DEFAULT(num_favorites),
    DB_FIELD_NOTNULL_NO_DEFAULT(num_list_users),
    DB_FIELD_NOTNULL_NO_DEFAULT(num_scoring_users),
    DB_FIELD_NOTNULL_NO_DEFAULT(popularity),
    DB_FIELD_NULLABLE_NO_DEFAULT(rank),
    DB_FIELD_NULLABLE_NO_DEFAULT(start_date),
    DB_FIELD_NULLABLE_NO_DEFAULT(start_season),
    DB_FIELD_NOTNULL_NO_DEFAULT(status),
    DB_FIELD_NOTNULL_NO_DEFAULT(synopsis),
    DB_FIELD_NOTNULL_NO_DEFAULT(title),
    DB_FIELD_NOTNULL_NO_DEFAULT(updated_at),
    DB_FIELD_NULLABLE_NO_DEFAULT(background)
  )
};

struct MALListStatusTable
{
  DBInteger id;
  DBText comments;
  DBInteger is_rewatching;
  DBInteger num_episodes_watched;
  std::optional<DBInteger> num_times_rewatched;
  DBInteger priority;
  DBInteger rewatch_value;
  DBReal score;
  DBInteger status;
  DBText tags;
  DBText updated_at;
  std::optional<DBText> start_date;
  std::optional<DBText> finish_date;

  DB_STRUCT(MALListStatusTable, "mal_list_status",
    DB_FIELD_ID(id),
    DB_FIELD_NOTNULL_NO_DEFAULT(comments),
    DB_FIELD_NOTNULL_NO_DEFAULT(is_rewatching),
    DB_FIELD_NOTNULL_NO_DEFAULT(num_episodes_watched),
    DB_FIELD_NULLABLE_NO_DEFAULT(num_times_rewatched),
    DB_FIELD_NOTNULL_NO_DEFAULT(priority),
    DB_FIELD_NOTNULL_NO_DEFAULT(rewatch_value),
    DB_FIELD_NOTNULL_NO_DEFAULT(score),
    DB_FIELD_NOTNULL_NO_DEFAULT(status),
    DB_FIELD_NOTNULL_NO_DEFAULT(tags),
    DB_FIELD_NOTNULL_NO_DEFAULT(updated_at),
    DB_FIELD_NULLABLE_NO_DEFAULT(start_date),
    DB_FIELD_NULLABLE_NO_DEFAULT(finish_date),
    DB_FOREIGN_KEY(id, MALAnimeTable, id, ForeignKeyAction::Cascade, ForeignKeyAction::Cascade)
  );
};

struct AnimeTable
{
  DBInteger id;
  std::optional<DBInteger> mal_id;
  std::optional<DBInteger> kitsu_id;

  DB_STRUCT(AnimeTable, "anime",
    DB_FIELD_ID_AUTO(id),
    DB_FIELD_NULLABLE_NO_DEFAULT(mal_id),
    DB_FIELD_NULLABLE_NO_DEFAULT(kitsu_id),
    DB_FOREIGN_KEY(mal_id, MALAnimeTable, id, ForeignKeyAction::SetNull, ForeignKeyAction::Cascade)
  )
};


}

#endif //ANIME_COMMAND_CENTER_TABLES_HPP
