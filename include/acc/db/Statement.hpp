//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_STATEMENT_HPP
#define ANIME_COMMAND_CENTER_STATEMENT_HPP

#include <acc/common/common.hpp>
#include <acc/db/db_struct_impl/types.hpp>

struct sqlite3_stmt;

namespace acc::db
{

class Database;

class Statement
{
public:
  explicit Statement(Database &, const std::string & query);

  ~Statement();

  DISABLE_COPY(Statement);

  Statement(Statement && other) noexcept
  {
    if (this != &other) {
      m_stmt = other.m_stmt;
      other.m_stmt = nullptr;
    }
  }

  Statement & operator=(Statement && other) noexcept
  {
    if (this != &other) {
      m_stmt = other.m_stmt;
      other.m_stmt = nullptr;
    }
    return *this;
  }

  ResultRows execute(Database &);

  void reset();

  void clear_bindings();

  void bind(int bind_index, DBInteger);

  void bind(int bind_index, DBReal);

  void bind(int bind_index, const DBText &);

  void bind(int bind_index, const DBBlob &);

  template<typename T>
  void bind(int bind_index, const std::optional<T> & value)
  {
    if (!value.has_value()) {
      bind_null(bind_index);
    } else {
      bind(bind_index, *value);
    }
  }

  void bind_null(int bind_index);

  [[nodiscard]] std::string sql() const;

private:
  sqlite3_stmt * m_stmt;
};

}

#endif //ANIME_COMMAND_CENTER_STATEMENT_HPP
