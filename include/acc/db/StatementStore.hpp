//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_STATEMENTSTORE_HPP
#define ANIME_COMMAND_CENTER_STATEMENTSTORE_HPP

#include <acc/db/Statement.hpp>

namespace acc::db
{

struct StatementStore
{
  Statement insert;
  Statement list;
  Statement get;
  Statement get_last_id;
  Statement update;
  Statement deleter;
  Statement upsert;
  Statement count;
};

}

#endif //ANIME_COMMAND_CENTER_STATEMENTSTORE_HPP
