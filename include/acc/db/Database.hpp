//
// Created by joncp on 5/11/2020.
//

#ifndef ANIME_COMMAND_CENTER_DB_DATABASE_HPP
#define ANIME_COMMAND_CENTER_DB_DATABASE_HPP

#include <acc/common/common.hpp>
#include <acc/common/exceptions.hpp>
#include <acc/db/exceptions.hpp>
#include <acc/db/db_struct_impl/types.hpp>
#include <acc/db/StatementStore.hpp>

#include <cstddef>

#include <set>

struct sqlite3;

namespace acc::db
{

class Database
{
public:
  explicit Database(const std::string & database_file);

  ~Database();

  DISABLE_COPY(Database);

  Database(Database && other) noexcept
  {
    if (this != &other) {
      m_db = other.m_db;
      other.m_db = nullptr;
    }
  }

  Database & operator=(Database && other) noexcept
  {
    if (this != &other) {
      m_db = other.m_db;
      other.m_db = nullptr;
    }
    return *this;
  }

  ResultRows execute_query(const std::string &);

  size_t get_schema_version();

  std::set<std::string> current_tables();

  bool table_exists(const std::string &);

  template<typename Table>
  void create_table()
  {
    execute_query(Table::create_table_query());
  }

  template<typename Table>
  bool create_table_if_not_exist()
  {
    if (!table_exists(Table::table_name)) {
      create_table<Table>();
      return true;
    }
    return false;
  }

  template<typename Table>
  bool register_table()
  {
    const auto r = create_table_if_not_exist<Table>();
    m_statement_store.emplace(Table::table_name, Table::construct_statement_store(*this));
    return r;
  }

  template<typename Table>
  StatementStore & statement_store()
  {
    return m_statement_store.at(Table::table_name);
  }  void begin_transaction()
  {
    execute_query("BEGIN TRANSACTION;");
  }

  void end_transaction()
  {
    execute_query("COMMIT TRANSACTION;");
  }

  void rollback_transaction()
  {
    execute_query("ROLLBACK;");
  }

  class TransactionGuard
  {
  public:
    explicit TransactionGuard(Database & db)
      : m_db(db),
        m_did_rollback(false)
    {
      m_db.begin_transaction();
    }

    DISABLE_COPY(TransactionGuard);

    DISABLE_MOVE(TransactionGuard);

    void rollback()
    {
      if (!m_did_rollback) {
        m_did_rollback = true;
        m_db.rollback_transaction();
      }
    }

    ~TransactionGuard()
    {
      if (!m_did_rollback) {
        m_db.end_transaction();
      }
    }

  private:
    Database & m_db;
    bool m_did_rollback;
  };

  TransactionGuard transaction()
  {
    return TransactionGuard(*this);
  }

private:
  sqlite3 * m_db;
  std::unordered_map<std::string, StatementStore> m_statement_store;

  friend class Statement;
};

}

#endif //ANIME_COMMAND_CENTER_DB_DATABASE_HPP
