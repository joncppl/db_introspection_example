//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_META_HPP
#define ANIME_COMMAND_CENTER_META_HPP

#include <acc/db/db_struct_impl/declaration.hpp>

namespace acc::db
{

struct Meta
{
  DBInteger id;
  DBInteger schema_version;

  DB_STRUCT(Meta, "META",
    DB_FIELD_ID_AUTO(id),
    DB_FIELD_NOTNULL_NO_DEFAULT(schema_version)
  );
};

constexpr const int64_t SCHEMA_VERSION = 0;

}

#endif //ANIME_COMMAND_CENTER_META_HPP
