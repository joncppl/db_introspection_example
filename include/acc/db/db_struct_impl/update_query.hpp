//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_UPDATE_QUERY_HPP
#define ANIME_COMMAND_CENTER_UPDATE_QUERY_HPP

#include <acc/db/db_struct_impl/types.hpp>

#include <sstream>

namespace acc::db::impl
{

namespace update_query_impl
{

template<typename FieldTypes, size_t I>
constexpr bool do_print()
{
  constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
  constexpr auto real_index = tuple_size - I;
  using field_info = std::tuple_element_t<real_index, FieldTypes>;
  if constexpr (field_info::info_type == InfoType::FieldInfo) {
    constexpr auto field_properties = field_info::field_properties;
    if (!FieldProperties_set(field_properties & FieldProperties::Auto)) {
      return true;
    }
  }
  return false;
}

template<typename FieldTypes, size_t I>
struct FieldUpdatePrinter
{
  static void print(std::stringstream & ss)
  {
    constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
    constexpr auto real_index = tuple_size - I;
    using field_info = std::tuple_element_t<real_index, FieldTypes>;

    using struct_type = typename field_info::struct_type;
    using db_info = typename struct_type::template DBInfo_<struct_type>;
    const auto & field_info_value = std::get<real_index>(db_info::meta());

    if constexpr (do_print<FieldTypes, I>()) {
      ss << '`' << field_info_value.name << "`=?";

      if constexpr (I > 1) {
        if constexpr (do_print<FieldTypes, I - 1>()) {
          ss << ", ";
        }
      }
    }
    FieldUpdatePrinter<FieldTypes, I - 1>::print(ss);
  }
};

template<typename FieldTypes>
struct FieldUpdatePrinter<FieldTypes, 0>
{
  static void print(std::stringstream &)
  {}
};

}

template<typename FieldTypes, size_t get_field_index, bool include_table_name = true>
std::string update_query(std::string_view table_name)
{
  std::stringstream ss;

  if constexpr (include_table_name) {
    ss << "UPDATE `" << table_name << "` SET ";
  } else {
    ss << "UPDATE SET ";
  }

  update_query_impl::FieldUpdatePrinter<FieldTypes, std::tuple_size_v<FieldTypes>>::print(ss);

  using struct_type = typename std::tuple_element_t<0, FieldTypes>::struct_type;
  const auto & field_info_value = std::get<get_field_index>(
    struct_type::template DBInfo_<struct_type>::meta());

  ss << "WHERE `" << field_info_value.name << "`=?";

  ss << ';';

  return ss.str();
}

}

#endif //ANIME_COMMAND_CENTER_UPDATE_QUERY_HPP
