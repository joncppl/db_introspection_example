//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_GET_HPP
#define ANIME_COMMAND_CENTER_GET_HPP

#include <acc/db/db_struct_impl/types.hpp>

namespace acc::db::impl
{

namespace get_field_impl
{
template<typename FieldTypes, size_t I>
struct GetFieldGetter
{
  constexpr static size_t get()
  {
    constexpr auto real_index = std::tuple_size_v<FieldTypes> - I;
    using field_info = std::tuple_element_t<real_index, FieldTypes>;
    constexpr auto field_properties = field_info::field_properties;
    if constexpr (FieldProperties_set(field_properties & FieldProperties::GetField)) {
      return real_index;
    } else {
      return GetFieldGetter<FieldTypes, I - 1>::get();
    }
  }
};

template<typename FieldTypes>
struct GetFieldGetter<FieldTypes, 0>
{
  constexpr static size_t get()
  {
    return std::numeric_limits<size_t>::max();
  }
};
}

template<typename FieldTypes>
constexpr size_t get_field()
{
  constexpr auto r = get_field_impl::GetFieldGetter<FieldTypes, std::tuple_size_v<FieldTypes>>::get();
  static_assert(r != std::numeric_limits<size_t>::max(), "No Get Field :(");
  return r;
}

template<typename struct_type, size_t get_field_index>
std::string get_query()
{
  const auto & field_info_value = std::get<get_field_index>(
    struct_type::template DBInfo_<struct_type>::meta());
  return fmt::format("SELECT * FROM `{}` WHERE `{}`=?;",
    struct_type::table_name,
    field_info_value.name);
}

template<typename struct_type, size_t get_field_index>
std::string delete_query()
{
  const auto & field_info_value = std::get<get_field_index>(
    struct_type::template DBInfo_<struct_type>::meta());
  return fmt::format("DELETE FROM `{}` WHERE `{}`=?;",
    struct_type::table_name,
    field_info_value.name);
}

}

#endif //ANIME_COMMAND_CENTER_GET_HPP
