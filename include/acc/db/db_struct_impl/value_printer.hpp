//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_VALUE_PRINTER_HPP
#define ANIME_COMMAND_CENTER_VALUE_PRINTER_HPP

#include <acc/db/db_struct_impl/types.hpp>

#include <iomanip>
#include <sstream>

namespace acc::db::impl
{

struct ValuePrinter
{
  template<typename T>
  static void print(std::stringstream & ss, const std::optional<T> & value)
  {
    if (!value.has_value()) {
      print(ss, nullptr);
    } else {
      print(ss, *value);
    }
  }

  static void print(std::stringstream & ss, DBNull)
  {
    ss << "NULL";
  }

  static void print(std::stringstream & ss, DBInteger value)
  {
    ss << value;
  }

  static void print(std::stringstream & ss, DBReal value)
  {
    ss << value;
  }

  static void print(std::stringstream & ss, const DBText & value)
  {
    ss << '\'' << value << '\'';
  }

  static void print(std::stringstream & ss, const std::string_view & value)
  {
    ss << '\'' << value << '\'';
  }

  static void print(std::stringstream & ss, const DBBlob & value)
  {
    ss << "X'";
    const auto flags = ss.flags();
    for (const auto v : value) {
      ss << std::setw(2) << std::setfill('0') << std::hex << static_cast<int>(v);
    }
    ss.flags(flags);
    ss << '\'';
  }
};

}

#endif //ANIME_COMMAND_CENTER_VALUE_PRINTER_HPP
