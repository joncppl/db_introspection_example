//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_CREATE_TABLE_HPP
#define ANIME_COMMAND_CENTER_CREATE_TABLE_HPP

#include <acc/db/db_struct_impl/types.hpp>
#include <acc/db/db_struct_impl/value_printer.hpp>

#include <sstream>

namespace acc::db::impl
{

namespace create_table_impl
{

template<ForeignKeyAction fka>
void foreign_key_actions(std::stringstream & ss)
{
  if constexpr (ForeignKeyAction_set(fka & ForeignKeyAction::NoAction)) {
    ss << ' ' << ForeignKeyAction_to_string(ForeignKeyAction::NoAction);
  }
  if constexpr (ForeignKeyAction_set(fka & ForeignKeyAction::Restrict)) {
    ss << ' ' << ForeignKeyAction_to_string(ForeignKeyAction::Restrict);
  }
  if constexpr (ForeignKeyAction_set(fka & ForeignKeyAction::SetNull)) {
    ss << ' ' << ForeignKeyAction_to_string(ForeignKeyAction::SetNull);
  }
  if constexpr (ForeignKeyAction_set(fka & ForeignKeyAction::SetDefault)) {
    ss << ' ' << ForeignKeyAction_to_string(ForeignKeyAction::SetDefault);
  }
  if constexpr (ForeignKeyAction_set(fka & ForeignKeyAction::Cascade)) {
    ss << ' ' << ForeignKeyAction_to_string(ForeignKeyAction::Cascade);
  }
}

template<typename FieldTypes, size_t I>
struct FieldPrinter
{
  static void print(std::stringstream & ss)
  {
    constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
    constexpr auto real_index = tuple_size - I;
    using field_info = std::tuple_element_t<real_index, FieldTypes>;

    using struct_type = typename field_info::struct_type;
    using db_info = typename struct_type::template DBInfo_<struct_type>;
    const auto & field_info_value = std::get<real_index>(db_info::meta());

    if constexpr (field_info::info_type == InfoType::FieldInfo) {
      constexpr auto field_properties = field_info::field_properties;

      ss << '`' << field_info_value.name << "` ";
      ss << DatabaseFieldType_to_string(field_info::db_field_type);

      if constexpr (FieldProperties_set(field_properties & FieldProperties::PrimaryKeyAsc)) {
        ss << " PRIMARY KEY ASC";
      } else if constexpr (FieldProperties_set(
        field_properties & FieldProperties::PrimaryKeyDesc)) {
        ss << " PRIMARY KEY DESC";
      }

      if constexpr (FieldProperties_set(field_properties & FieldProperties::Unique)) {
        ss << " UNIQUE";
      }

      if constexpr (FieldProperties_set(field_properties & FieldProperties::NotNull)) {
        ss << " NOT NULL";
      } else {
        ss << " NULL";
      }

      if (field_info_value.default_value.has_value()) {
        ss << " DEFAULT ";
        ValuePrinter::print(ss, *field_info_value.default_value);
      }

      if constexpr (I > 1) {
        ss << ", ";
      }

    } else if constexpr (field_info::info_type == InfoType::ForeignKeyConstraint) {
      ss << " FOREIGN KEY(`" << field_info_value.name << "`) REFERENCES ";
      ss << field_info::other_struct_type::table_name << "(";
      ss << field_info_value.other_name << ")";

      if constexpr (ForeignKeyAction_set(field_info::on_delete)) {
        ss << " ON DELETE";
        foreign_key_actions<field_info::on_delete>(ss);
      }

      if constexpr (ForeignKeyAction_set(field_info::on_update)) {
        ss << " ON UPDATE";
        foreign_key_actions<field_info::on_update>(ss);
      }

      if constexpr (I > 1) {
        ss << ", ";
      }
    }

    FieldPrinter<FieldTypes, I - 1>::print(ss);
  }
};

template<typename FieldTypes>
struct FieldPrinter<FieldTypes, 0>
{
  static void print(std::stringstream &)
  {}
};

}

template<typename FieldTypes>
std::string create_table_query(std::string_view table_name)
{
  std::stringstream ss;

  ss << "CREATE TABLE `" << table_name << "` (";

  create_table_impl::FieldPrinter<FieldTypes, std::tuple_size_v<FieldTypes>>::print(ss);

  ss << ");";

  return ss.str();
}

}

#endif //ANIME_COMMAND_CENTER_CREATE_TABLE_HPP
