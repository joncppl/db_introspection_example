//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_INSERT_QUERY_HPP
#define ANIME_COMMAND_CENTER_INSERT_QUERY_HPP

#include <acc/db/db_struct_impl/types.hpp>
#include <acc/db/Database.hpp>

#include <sstream>

namespace acc::db::impl
{

namespace insert_query_impl
{

template<typename FieldTypes, size_t I>
constexpr bool do_print()
{
  constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
  constexpr auto real_index = tuple_size - I;
  using field_info = std::tuple_element_t<real_index, FieldTypes>;
  if constexpr (field_info::info_type == InfoType::FieldInfo) {
    constexpr auto field_properties = field_info::field_properties;
    if (!FieldProperties_set(field_properties & FieldProperties::Auto)) {
      return true;
    }
  }
  return false;
}

template<typename FieldTypes, size_t I>
struct FieldNamePrinter
{
  static void print(std::stringstream & ss, size_t & q_count)
  {
    constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
    constexpr auto real_index = tuple_size - I;
    using field_info = std::tuple_element_t<real_index, FieldTypes>;

    using struct_type = typename field_info::struct_type;
    using db_info = typename struct_type::template DBInfo_<struct_type>;
    const auto & field_info_value = std::get<real_index>(db_info::meta());

    if constexpr (do_print<FieldTypes, I>()) {
      ss << '`' << field_info_value.name << '`';
      q_count++;

      if constexpr (I > 1) {
        if constexpr (do_print<FieldTypes, I - 1>()) {
          ss << ", ";
        }
      }

    }

    FieldNamePrinter<FieldTypes, I - 1>::print(ss, q_count);
  }
};

template<typename FieldTypes>
struct FieldNamePrinter<FieldTypes, 0>
{
  static void print(std::stringstream &, size_t &)
  {}
};

}

template<typename FieldTypes>
std::string insert_query(std::string_view table_name)
{
  std::stringstream ss;

  ss << "INSERT INTO `" << table_name << "` (";

  size_t q_count = 0;
  insert_query_impl::FieldNamePrinter<FieldTypes, std::tuple_size_v<FieldTypes>>::print(ss,
    q_count);

  ss << ") VALUES (";

  for (size_t i = 0; i < q_count; i++) {
    ss << "?";
    if (i < q_count - 1)
      ss << ", ";
  }

  ss << ");";

  return ss.str();
}

namespace bind_insert_query_impl
{
template<typename FieldTypes, typename struct_type, size_t I>
struct FieldBinder
{
  static void bind(const struct_type & s, Statement & statement, int & bind_index)
  {
    constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
    constexpr auto real_index = tuple_size - I;
    using field_info = std::tuple_element_t<real_index, FieldTypes>;

    constexpr auto member_ptr = field_info::member_ptr;

    if constexpr (field_info::info_type == InfoType::FieldInfo) {
      constexpr auto field_properties = field_info::field_properties;
      if (!FieldProperties_set(field_properties & FieldProperties::Auto)) {
        using db_info = typename struct_type::template DBInfo_<struct_type>;
        const auto & field_info_value = std::get<real_index>(db_info::meta());

        statement.bind(bind_index++, s.*(member_ptr));
      }
    }
    FieldBinder<FieldTypes, struct_type, I - 1>::bind(s, statement, bind_index);
  }
};

template<typename FieldTypes, typename struct_type>
struct FieldBinder<FieldTypes, struct_type, 0>
{
  static void bind(const struct_type &, Statement &, int &)
  {}
};

}

template<typename FieldTypes, typename struct_type>
int bind_insert_query(const struct_type & s, Statement & statement, int initial_bind_index = 1)
{
  int bind_index = initial_bind_index;
  bind_insert_query_impl::FieldBinder<FieldTypes, struct_type, std::tuple_size_v<FieldTypes>>::bind(
    s, statement, bind_index);
  return bind_index;
}

}

#endif //ANIME_COMMAND_CENTER_INSERT_QUERY_HPP
