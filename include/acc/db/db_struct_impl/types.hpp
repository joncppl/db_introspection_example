//
// Created by Jonathan on 9/26/2020.
//

#ifndef ANIME_COMMAND_CENTER_DB_STRUCT_HPP
#define ANIME_COMMAND_CENTER_DB_STRUCT_HPP

#include <cstddef>

#include <algorithm>
#include <iterator>
#include <optional>
#include <string>
#include <variant>
#include <vector>
#include <unordered_map>

namespace acc::db
{

class PreparedStatement;

using DBNull = nullptr_t;
using DBInteger = int64_t;
using DBReal = double;
using DBText = std::string;
using DBBlob = std::vector<uint8_t>;

using FieldValue = std::variant<DBNull,
  DBInteger,
  DBReal,
  DBText,
  DBBlob,
  std::optional<DBInteger>,
  std::optional<DBReal>,
  std::optional<DBText>,
  std::optional<DBBlob>>;
using RowResult = std::unordered_map<std::string, FieldValue>;
using ResultRows = std::vector<RowResult>;

enum class DatabaseFieldType
{
  Unknown,
  Null,
  Integer,
  Real,
  Text,
  Blob
};

constexpr const char DB_TYPE_STRING_UNKNOWN[] = "UNKNOWN";
constexpr const char DB_TYPE_STRING_NULL[] = "NULL";
constexpr const char DB_TYPE_STRING_INTEGER[] = "INTEGER";
constexpr const char DB_TYPE_STRING_REAL[] = "REAL";
constexpr const char DB_TYPE_STRING_TEXT[] = "TEXT";
constexpr const char DB_TYPE_STRING_BLOB[] = "BLOB";

constexpr const char * DatabaseFieldType_to_string(DatabaseFieldType type)
{
  switch (type) {
    default:
    case DatabaseFieldType::Unknown:
      return DB_TYPE_STRING_UNKNOWN;
    case DatabaseFieldType::Null:
      return DB_TYPE_STRING_NULL;
    case DatabaseFieldType::Integer:
      return DB_TYPE_STRING_INTEGER;
    case DatabaseFieldType::Real:
      return DB_TYPE_STRING_REAL;
    case DatabaseFieldType::Text:
      return DB_TYPE_STRING_TEXT;
    case DatabaseFieldType::Blob:
      return DB_TYPE_STRING_BLOB;
  }
}

inline DatabaseFieldType DatabaseFieldType_from_string(const std::string_view & type)
{
  std::string upper;
  std::transform(type.begin(), type.end(), std::back_inserter(upper), ::toupper);
  if (upper == DB_TYPE_STRING_NULL) {
    return DatabaseFieldType::Null;
  } else if (upper == DB_TYPE_STRING_INTEGER) {
    return DatabaseFieldType::Integer;
  } else if (upper == DB_TYPE_STRING_REAL) {
    return DatabaseFieldType::Real;
  } else if (upper == DB_TYPE_STRING_TEXT) {
    return DatabaseFieldType::Text;
  } else if (upper == DB_TYPE_STRING_BLOB) {
    return DatabaseFieldType::Blob;
  }
  return DatabaseFieldType::Unknown;
}

enum class FieldProperties
{
  None = 0x00,
  PrimaryKeyAsc = 0x01,
  PrimaryKeyDesc = 0x02,
  NotNull = 0x04,
  Unique = 0x08,
  GetField = 0x10,
  Auto = 0x20,
};

inline constexpr FieldProperties operator|(FieldProperties lhs, FieldProperties rhs)
{
  using ul = std::underlying_type_t<FieldProperties>;
  return static_cast<FieldProperties>(static_cast<ul>(lhs) | static_cast<ul>(rhs));
}

inline constexpr FieldProperties operator&(FieldProperties lhs, FieldProperties rhs)
{
  using ul = std::underlying_type_t<FieldProperties>;
  return static_cast<FieldProperties>(static_cast<ul>(lhs) & static_cast<ul>(rhs));
}

inline constexpr bool FieldProperties_set(FieldProperties fp)
{
  using ul = std::underlying_type_t<FieldProperties>;
  return static_cast<ul>(fp) != 0;
}

enum class ForeignKeyAction
{
  Undefined = 0,
  NoAction = 0x01,
  Restrict = 0x02,
  SetNull = 0x04,
  SetDefault = 0x08,
  Cascade = 0x10
};

constexpr const char * ForeignKeyAction_to_string(ForeignKeyAction fka)
{
  switch (fka) {
    case ForeignKeyAction::NoAction:
      return "NO ACTION";
    case ForeignKeyAction::Restrict:
      return "RESTRICT";
    case ForeignKeyAction::SetNull:
      return "SET NULL";
    case ForeignKeyAction::SetDefault:
      return "SET DEFAULT";
    case ForeignKeyAction::Cascade:
      return "CASCADE";
    default:
    case ForeignKeyAction::Undefined:
      break;
  }
  return "UNDEFINED";
}

inline constexpr ForeignKeyAction operator|(ForeignKeyAction lhs, ForeignKeyAction rhs)
{
  using ul = std::underlying_type_t<ForeignKeyAction>;
  return static_cast< ForeignKeyAction>(static_cast<ul>(lhs) | static_cast<ul>(rhs));
}

inline constexpr ForeignKeyAction operator&(ForeignKeyAction lhs, ForeignKeyAction rhs)
{
  using ul = std::underlying_type_t<ForeignKeyAction>;
  return static_cast< ForeignKeyAction>(static_cast<ul>(lhs) & static_cast<ul>(rhs));
}

inline constexpr bool ForeignKeyAction_set(ForeignKeyAction fp)
{
  using ul = std::underlying_type_t<ForeignKeyAction>;
  return static_cast<ul>(fp) != 0;
}

template<typename T, typename Enable = void>
struct is_optional : std::false_type
{
};

template<typename T>
struct is_optional<std::optional<T> > : std::true_type
{
};

template<typename T>
constexpr DatabaseFieldType database_field_type()
{
  if constexpr (is_optional<T>::value) {
    return database_field_type<typename T::value_type>();
  } else if constexpr (std::is_same_v<T, DBNull>) {
    return DatabaseFieldType::Null;
  } else if constexpr (std::is_same_v<T, DBInteger>) {
    return DatabaseFieldType::Integer;
  } else if constexpr (std::is_same_v<T, DBReal>) {
    return DatabaseFieldType::Real;
  } else if constexpr (std::is_same_v<T, DBText>) {
    return DatabaseFieldType::Text;
  } else if constexpr (std::is_same_v<T, DBBlob>) {
    return DatabaseFieldType::Blob;
  } else {
    static_assert(sizeof(T) != sizeof(T), "Not a valid DB type");
  }
}

namespace impl
{

enum class InfoType
{
  FieldInfo,
  ForeignKeyConstraint
};

template<typename T>
struct default_element_type
{
};

template<typename T>
struct default_element_type<std::optional<T>>
{
  using type = typename default_element_type<T>::type;
};

template<>
struct default_element_type<DBText>
{
  using type = std::string_view;
};

template<>
struct default_element_type<DBInteger>
{
  using type = DBInteger;
};

template<>
struct default_element_type<DBReal>
{
  using type = DBReal;
};


template<
  typename struct_,
  typename element_,
  element_ struct_::*member_ptr_,
  FieldProperties field_properties_
>
struct FieldInfo
{
  constexpr static InfoType info_type = InfoType::FieldInfo;

  std::string_view name;

  using struct_type = struct_;
  using element_type = element_;
  constexpr static auto db_field_type = database_field_type<element_type>();
  constexpr static element_type struct_type::*member_ptr = member_ptr_;
  constexpr static FieldProperties field_properties = field_properties_;

  static_assert(!(FieldProperties_set(field_properties & FieldProperties::PrimaryKeyAsc) &&
                  FieldProperties_set(field_properties & FieldProperties::PrimaryKeyDesc)),
    "Field Primary Key can't be both Asc and Desc");

  static_assert((!FieldProperties_set(field_properties & FieldProperties::NotNull) &&
                 is_optional<element_type>::value) ||
                (FieldProperties_set(field_properties & FieldProperties::NotNull) &&
                 !is_optional<element_type>::value), "Nullable fields must be std::optional");

  using default_value_type = std::optional<typename default_element_type<element_type>::type>;
  default_value_type default_value;

  constexpr FieldInfo(
    std::string_view name,
    const default_value_type & default_value
  ) : name(name),
      default_value(default_value)
  {}
};

template<
  typename struct_,
  typename element_,
  element_ struct_::*member_ptr_,
  FieldProperties field_properties_
>
constexpr auto
make_field_info_(std::string_view name, const std::optional<element_> & default_value)
{
  return FieldInfo<struct_, element_, member_ptr_, field_properties_>(
    name,
    default_value
  );
}

template<
  typename struct_,
  typename element_,
  element_ struct_::*member_ptr_,
  FieldProperties field_properties_
>
constexpr auto make_field_info_(std::string_view name, std::nullptr_t)
{
  return FieldInfo<struct_, element_, member_ptr_, field_properties_>(
    name,
    std::nullopt
  );
}

template<
  typename struct_,
  typename element_,
  element_ struct_::*member_ptr_,
  typename other_struct_,
  typename other_element_,
  other_element_ other_struct_::*other_member_ptr_,
  ForeignKeyAction on_delete_,
  ForeignKeyAction on_update_
>
struct ForeignKey
{
  constexpr static InfoType info_type = InfoType::ForeignKeyConstraint;

  using struct_type = struct_;
  using element_type = element_;
  constexpr static auto db_field_type = database_field_type<element_type>();
  constexpr static element_type struct_type::*member_ptr = member_ptr_;

  using other_struct_type = other_struct_;
  using other_element_type = other_element_;
  constexpr static auto other_db_field_type = database_field_type<other_element_type>();
  constexpr static other_element_type other_struct_type::*other_member_ptr = other_member_ptr_;

  constexpr static ForeignKeyAction on_delete = on_delete_;
  constexpr static ForeignKeyAction on_update = on_update_;


  std::string_view name;
  std::string_view other_name;

  constexpr ForeignKey(std::string_view name, std::string_view other_name)
    : name(name),
      other_name(other_name)
  {}
};

template<
  typename struct_,
  typename element_,
  element_ struct_::*member_ptr_,
  typename other_struct_,
  typename other_element_,
  other_element_ other_struct_::*other_member_ptr_,
  ForeignKeyAction on_delete_,
  ForeignKeyAction on_update_
>
constexpr auto make_foreign_key_(std::string_view name, std::string_view other_name)
{
  return ForeignKey<struct_,
    element_,
    member_ptr_,
    other_struct_,
    other_element_,
    other_member_ptr_,
    on_delete_,
    on_update_>(name, other_name);
}

}

}

#endif //ANIME_COMMAND_CENTER_DB_STRUCT_HPP
