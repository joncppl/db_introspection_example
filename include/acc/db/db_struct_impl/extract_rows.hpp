//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_EXTRACT_ROWS_HPP
#define ANIME_COMMAND_CENTER_EXTRACT_ROWS_HPP

#include <acc/db/db_struct_impl/types.hpp>

namespace acc::db::impl
{

namespace extract_row_impl
{

template<typename FieldTypes, typename struct_type, size_t I>
struct FieldExtractor
{
  static void extract(const RowResult & row, struct_type & s)
  {
    constexpr auto tuple_size = std::tuple_size_v<FieldTypes>;
    constexpr auto real_index = tuple_size - I;
    using field_info = std::tuple_element_t<real_index, FieldTypes>;

    using db_info = typename struct_type::template DBInfo_<struct_type>;
    const auto & field_info_value = std::get<real_index>(db_info::meta());
    constexpr auto member_ptr = field_info::member_ptr;

    if constexpr (field_info::info_type == InfoType::FieldInfo) {
      const auto maybe_value = row.at(field_info_value.name.data());
      if constexpr (is_optional<typename field_info::element_type>::value) {
        if (std::holds_alternative<std::nullptr_t>(maybe_value)) {
           s.*(member_ptr) = std::nullopt;
        } else {
          s.*(member_ptr) = std::get<typename field_info::element_type::value_type>(maybe_value);
        }
      } else {
        s.*(member_ptr) = std::get<typename field_info::element_type>(maybe_value);
      }
    }

    FieldExtractor<FieldTypes, struct_type, I - 1>::extract(row, s);
  }
};

template<typename FieldTypes, typename struct_type>
struct FieldExtractor<FieldTypes, struct_type, 0>
{
  static void extract(const RowResult &, struct_type &)
  {}
};

}

template<typename FieldTypes, typename struct_type>
struct_type extract_row(const RowResult & row)
{
  struct_type s;
  extract_row_impl::FieldExtractor<FieldTypes, struct_type, std::tuple_size_v<FieldTypes>>::extract(
    row,
    s);
  return s;
}

template<typename FieldTypes, typename struct_type>
std::vector<struct_type> extract_rows(const ResultRows & rows)
{
  std::vector<struct_type> ret;
  std::transform(rows.begin(),
    rows.end(),
    std::back_inserter(ret),
    extract_row<FieldTypes, struct_type>);
  return ret;
}
}

#endif //ANIME_COMMAND_CENTER_EXTRACT_ROWS_HPP
