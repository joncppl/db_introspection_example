//
// Created by Jonathan on 9/27/2020.
//

#ifndef ANIME_COMMAND_CENTER_DECLARATION_HPP
#define ANIME_COMMAND_CENTER_DECLARATION_HPP

#include <acc/db/Database.hpp>
#include <acc/db/db_struct_impl/types.hpp>
#include <acc/db/db_struct_impl/create_table.hpp>
#include <acc/db/db_struct_impl/insert_query.hpp>
#include <acc/db/db_struct_impl/extract_rows.hpp>
#include <acc/db/db_struct_impl/get.hpp>
#include <acc/db/db_struct_impl/update_query.hpp>

namespace acc::db::impl
{
template<typename StructT, typename FieldTypes_, typename C>
struct DBInfo
{
  using FieldTypes = FieldTypes_;

  constexpr static size_t GetFieldIndex = acc::db::impl::get_field<FieldTypes>();

  using GetFieldTypeInfo = typename std::tuple_element_t<GetFieldIndex, FieldTypes>;

  using GetFieldType = typename GetFieldTypeInfo::element_type;

  static std::string create_table_query()
  {
    return acc::db::impl::create_table_query<FieldTypes>(C::name);
  }

  static std::string insert_query()
  {
    return acc::db::impl::insert_query<FieldTypes>(C::name);
  }

  static std::string list_query()
  {
    return fmt::format("SELECT * from `{}`;", C::name);
  }

  static void bind_insert_query(const StructT & s, Statement & statement)
  {
    acc::db::impl::bind_insert_query<FieldTypes, StructT>(s, statement);
  }

  static void bind_update_query(const StructT & s, Statement & statement)
  {
    statement.bind(acc::db::impl::bind_insert_query<FieldTypes, StructT>(s, statement),
      s.*(GetFieldTypeInfo::member_ptr));
  }

  static void bind_upsert_query(const StructT & s, Statement & statement)
  {
    statement.bind(acc::db::impl::bind_insert_query<FieldTypes, StructT>(s,
      statement,
      acc::db::impl::bind_insert_query<FieldTypes, StructT>(s, statement)),
      s.*(GetFieldTypeInfo::member_ptr));
  }

  static std::vector<StructT> extract_list(const ResultRows & rows)
  {
    return acc::db::impl::extract_rows<FieldTypes, StructT>(rows);
  }

  static std::string get_query()
  {
    return acc::db::impl::get_query<StructT, GetFieldIndex>();
  }

  static std::string delete_query()
  {
    return acc::db::impl::delete_query<StructT, GetFieldIndex>();
  }

  static std::string update_query()
  {
    return acc::db::impl::update_query<FieldTypes, GetFieldIndex>(C::name);
  }

  static std::string upsert_query()
  {
    const auto & field_info_value = std::get<GetFieldIndex>(C::meta());
    const auto iq = insert_query();
    return fmt::format("{} ON CONFLICT(`{}`) DO {}",
      std::string_view(iq.c_str(), iq.size() - 1),
      field_info_value.name,
      acc::db::impl::update_query<FieldTypes, GetFieldIndex, false>(C::name));
  }

  static std::string count_query()
  {
    return fmt::format("SELECT COUNT(*) FROM `{}`;", C::name);
  }
};
}

#define DB_FIELD(element, field_properties, default_value) \
  acc::db::impl::make_field_info_<StructT, decltype(StructT::element), &StructT::element, field_properties>(#element, default_value)

#define DB_FIELD_ID(element) DB_FIELD(element, FieldProperties::NotNull | FieldProperties::PrimaryKeyAsc | FieldProperties::Unique | FieldProperties::GetField, nullptr)

#define DB_FIELD_ID_AUTO(element) DB_FIELD(element, FieldProperties::NotNull | FieldProperties::PrimaryKeyAsc | FieldProperties::Unique | FieldProperties::GetField | FieldProperties::Auto, nullptr)

#define DB_FIELD_NULLABLE_NO_DEFAULT(element) DB_FIELD(element, FieldProperties::None, nullptr)

#define DB_FIELD_NOTNULL_NO_DEFAULT(element) DB_FIELD(element, FieldProperties::NotNull, nullptr)

#define DB_FOREIGN_KEY(element, other_struct, other_element, on_delete, on_update) \
  acc::db::impl::make_foreign_key_<StructT, decltype(StructT::element), &StructT::element, other_struct, decltype(other_struct::other_element), &other_struct::other_element, on_delete, on_update>(#element, #other_element)

#define _META_DECL(...) \
 constexpr static const auto & meta() {return meta_;} \
  constexpr static const auto meta_ = std::make_tuple(__VA_ARGS__);

#define DB_STRUCT(T, name_, ...) \
  template<typename StructT>     \
  struct DBInfo_ : public acc::db::impl::DBInfo<T, decltype(std::make_tuple(__VA_ARGS__)), DBInfo_<T> > \
  {                              \
    constexpr static const char name[] = name_;                                                         \
    _META_DECL(__VA_ARGS__)      \
  };                             \
                                 \
  constexpr static const char table_name[] = name_;                                                     \
                                 \
  using GetFieldType_ = DBInfo_<T>::GetFieldType;                                                       \
                                 \
  /* Query Generators */         \
                                 \
  static std::string create_table_query()                                                               \
  {                              \
    return DBInfo_<T>::create_table_query();                                                            \
  }                              \
                                 \
  static std::string insert_query()                                                                     \
  {                              \
    return DBInfo_<T>::insert_query();                                                                  \
  }                              \
                                 \
  static std::string list_query() {                                                                     \
    return DBInfo_<T>::list_query();                                                                    \
  }                              \
                                 \
  static std::string get_query() \
  {                              \
    return DBInfo_<T>::get_query();                                                                     \
  }                              \
                                 \
  static std::string update_query()                                                                     \
  {                              \
    return DBInfo_<T>::update_query();                                                                  \
  }                              \
                                 \
  static std::string delete_query()                                                                     \
  {                              \
    return DBInfo_<T>::delete_query();                                                                  \
  }                              \
                                 \
  static std::string upsert_query()                                                                     \
  {                              \
    return DBInfo_<T>::upsert_query();                                                                  \
  }                              \
                                 \
  static std::string count_query()                                                                      \
  {                              \
    return DBInfo_<T>::count_query();                                                                   \
  }                              \
                                 \
  static acc::db::StatementStore construct_statement_store(acc::db::Database & db)                      \
  {                              \
    return StatementStore {      \
      acc::db::Statement(db, insert_query()),                                                           \
      acc::db::Statement(db, list_query()),                                                             \
      acc::db::Statement(db, get_query()),                                                              \
      acc::db::Statement(db,     \
        fmt::format("SELECT {} FROM {} WHERE ROWID=last_insert_rowid();",                               \
          std::get<DBInfo_<T>::GetFieldIndex>(DBInfo_<T>::meta()).name, table_name)),              \
      acc::db::Statement(db, update_query()),                                                           \
      acc::db::Statement(db, delete_query()),                                                           \
      acc::db::Statement(db, upsert_query()),                                                           \
      acc::db::Statement(db, count_query())                                                             \
    };                           \
  };                             \
                                 \
  /* Binders */                  \
                                 \
  void bind_insert_query(Statement & statement)                                                         \
  {                              \
    DBInfo_<T>::bind_insert_query(*this, statement);                                                    \
  }                              \
                                 \
  void bind_update_query(Statement & statement)                                                         \
  {                              \
    DBInfo_<T>::bind_update_query(*this, statement);                                                    \
  }                              \
                                 \
  void bind_upsert_query(Statement & statement)                                                         \
  {                              \
    DBInfo_<T>::bind_upsert_query(*this, statement);                                                    \
  }                              \
                                 \
  /* Ops */                      \
                                 \
  GetFieldType_ insert(acc::db::Database & database)                                                    \
  {                              \
    auto & store = database.statement_store<T>();                                                       \
    store.insert.reset();        \
    bind_insert_query(store.insert);                                                                    \
    store.insert.execute(database);                                                                     \
    store.get_last_id.reset();   \
    const auto ng = std::get<GetFieldType_>(                                                            \
      store.get_last_id.execute(database)[0][                                                           \
      std::get<DBInfo_<T>::GetFieldIndex>(DBInfo_<T>::meta()).name.data()                               \
    ]);                          \
    (*this).*(DBInfo_<T>::GetFieldTypeInfo::member_ptr) = ng;                                           \
    return ng;                   \
  }                              \
                                 \
  static std::vector<T> extract_list(const ResultRows & rows)                                           \
  {                              \
    return DBInfo_<T>::extract_list(rows);                                                              \
  }                              \
                                 \
  static std::vector<T> list(acc::db::Database & database)                                              \
  {                              \
    auto & stmt = database.statement_store<T>().list;                                                   \
    stmt.reset();                \
    return extract_list(stmt.execute(database));                                                        \
  }                              \
                                 \
  static std::optional<T> get(acc::db::Database & database, const GetFieldType_ & id)                   \
  {                              \
    auto & stmt = database.statement_store<T>().get;                                                    \
    stmt.reset();                \
    stmt.bind(1, id);            \
    const auto rr = stmt.execute(database);                                                             \
    if (rr.size() == 1) {        \
      return acc::db::impl::extract_row<DBInfo_<T>::FieldTypes, T>(rr[0]);                              \
    }                            \
    return std::nullopt;         \
  }                              \
                                 \
  void update(acc::db::Database & database)                                                             \
  {                              \
    auto & stmt = database.statement_store<T>().update;                                                 \
    stmt.reset();                \
    bind_update_query(stmt);     \
    stmt.execute(database);      \
  }                              \
                                 \
  void do_delete(acc::db::Database & database)                                                          \
  {                              \
    auto & stmt = database.statement_store<T>().deleter;                                                \
    stmt.reset();                \
    stmt.bind(1, (*this).*(DBInfo_<T>::GetFieldTypeInfo::member_ptr));                                  \
    stmt.execute(database);      \
  }                              \
                                 \
  std::optional<GetFieldType_> upsert(acc::db::Database & database)                                     \
  {                              \
    auto & store = database.statement_store<T>();                                                       \
    store.upsert.reset();        \
    bind_upsert_query(store.upsert);                                                                    \
    const auto count_ = count(database);                                                                \
    store.upsert.execute(database);                                                                     \
    if (count_ != count(database)) {                                                                    \
      store.get_last_id.reset(); \
      const auto ng = std::get<GetFieldType_>(                                                          \
        store.get_last_id.execute(database)[0][                                                         \
        std::get<DBInfo_<T>::GetFieldIndex>(DBInfo_<T>::meta()).name.data()                             \
      ]);                        \
      (*this).*(DBInfo_<T>::GetFieldTypeInfo::member_ptr) = ng;                                         \
      return ng;                 \
    }                            \
    return std::nullopt;         \
  }                              \
                                 \
  void upsert_fast(acc::db::Database & database)                                                        \
  {                              \
    auto & store = database.statement_store<T>();                                                       \
    store.upsert.reset();        \
    bind_upsert_query(store.upsert);                                                                    \
    store.upsert.execute(database);                                                                     \
  }                              \
                                 \
  static int64_t count(acc::db::Database & database)                                                    \
  {                              \
    auto & stmt = database.statement_store<T>().count;                                                  \
    stmt.reset();                \
    const auto rr = stmt.execute(database);                                                             \
    return std::get<DBInteger>(rr[0].begin()->second);                                                  \
  }

#endif //ANIME_COMMAND_CENTER_DECLARATION_HPP
