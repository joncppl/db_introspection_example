//
// Created by Jonathan on 8/30/2020.
//

#ifndef ANIME_COMMAND_CENTER_DB_EXCEPTIONS_HPP
#define ANIME_COMMAND_CENTER_DB_EXCEPTIONS_HPP

#include <acc/common/exceptions.hpp>

namespace acc::db
{
DEFINE_RUNTIME_ERROR(DatabaseError);

#define DEFINE_DATABASE_ERROR(cls) \
class cls : public DatabaseError \
{ \
public: \
  explicit cls(const std::string & msg) : DatabaseError(msg) \
  {spdlog::error(msg);} \
 \
  explicit cls(const char * msg) : DatabaseError(msg) \
  {spdlog::error(msg);} \
}

DEFINE_DATABASE_ERROR(DatabaseInitializeError);

DEFINE_DATABASE_ERROR(DatabaseNullStatementError);

DEFINE_DATABASE_ERROR(DatabaseStatementPrepareError);

DEFINE_DATABASE_ERROR(DatabaseStatementBindError);

DEFINE_DATABASE_ERROR(DatabaseStatementExecutionError);

DEFINE_DATABASE_ERROR(DatabaseConstraintError);

}

#endif //ANIME_COMMAND_CENTER_DB_EXCEPTIONS_HPP
